# My CV!

This repo contains my latest CV typeset using latex based on the amazing [Awesome-CV from Posqit0](https://github.com/posquit0/Awesome-CV).

An up to date copy of this CV in PDf format can always be found [here](https://gitlab.com/chemsmith/cv/-/jobs/artifacts/master/raw/cv.pdf?job=build) thanks to Gitlab CI.

## Building (compiling PDF)

### Requirements

- A recent version of the TexLive latex distribution installed (you'll likely require a -full TexLive distribution or manually selected extra packages as it does not (as of 5/2020) compile with a -minimal TexLive distribution)

### Compilation
`lualatex cv.tex`
